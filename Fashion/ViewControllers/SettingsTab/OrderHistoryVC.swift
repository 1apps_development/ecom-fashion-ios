//
//  OrderHistoryVC.swift
//  Fashion
//
//  Created by DREAMWORLD on 28/03/22.
//

import UIKit

class orderhistorycell : UITableViewCell
{
    
}
class OrderHistoryVC: UIViewController {

    @IBOutlet weak var Height_Tableview: NSLayoutConstraint!
    @IBOutlet weak var Tableview_Orderhistory: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.Tableview_Orderhistory.dataSource = self
        self.Tableview_Orderhistory.delegate = self
        self.Tableview_Orderhistory.reloadData()
        self.Height_Tableview.constant = 10 * 90
    }
    
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    


}
//MARK: Tableview Methods
extension OrderHistoryVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.Tableview_Orderhistory.dequeueReusableCell(withIdentifier: "orderhistorycell") as! orderhistorycell
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OrderhistoryDetailsVC") as! OrderhistoryDetailsVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
