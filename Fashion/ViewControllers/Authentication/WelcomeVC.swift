//
//  WelcomeVC.swift
//  Fashion
//
//  Created by DREAMWORLD on 23/03/22.
//

import UIKit

class WelcomeVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnTap_Login(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func btnTap_Signup(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func btnTap_Guest(_ sender: UIButton) {
        
        let vc = MainstoryBoard.instantiateViewController(withIdentifier: "MainTabViewController") as! MainTabViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
//        let objVC = MainstoryBoard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
//        let TabViewController = MainstoryBoard.instantiateViewController(withIdentifier: "MainTabViewController") as! MainTabViewController
//        let appNavigation: UINavigationController = UINavigationController(rootViewController: objVC)
//        appNavigation.setNavigationBarHidden(true, animated: true)
//        keyWindow?.rootViewController = TabViewController
    }

}
