//
//  AllCategoriesVC.swift
//  Fashion
//
//  Created by DREAMWORLD on 29/03/22.
//

import UIKit

class AllCategoriesVC: UIViewController {

    @IBOutlet weak var View_Circle: UIView!
    @IBOutlet weak var Height_collectionview: NSLayoutConstraint!
    @IBOutlet weak var Collectionview_CategoriesList: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.Collectionview_CategoriesList.delegate = self
        self.Collectionview_CategoriesList.dataSource = self
        self.Collectionview_CategoriesList.reloadData()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.Height_collectionview.constant = self.Collectionview_CategoriesList.contentSize.height
        }
        
        print("Click")
    }
    
    @IBAction func btnTap_Back(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.View_Circle.isHidden = false
    }
    
    @IBAction func btnTap_Close(_ sender: UIButton) {
        
        self.View_Circle.isHidden = true
    }
    
    @IBAction func btnTap_filter(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
        self.navigationController?.pushViewController(vc, animated: false)
    }

}
// MARK:- CollectionView Deleget methods
extension AllCategoriesVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.Collectionview_CategoriesList
        {
            return 10
        }
        else
        {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         if collectionView == self.Collectionview_CategoriesList
        {
            let cell = self.Collectionview_CategoriesList.dequeueReusableCell(withReuseIdentifier: "CategoriesCell", for: indexPath) as! CategoriesCell
            return cell
        }
        else
        {
            return UICollectionViewCell()
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         if collectionView == self.Collectionview_CategoriesList
        {
            return CGSize(width: (UIScreen.main.bounds.width - 54) / 2, height: 210)
//             return CGSize(width: (UIScreen.main.bounds.width - 54) / 2, height: 210)
        }
        else
        {
            return CGSize.zero
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

//        if collectionView == self.Collectionview_CategoriesList
//        {
//            let vc = self.storyboard?.instantiateViewController(identifier: "ItemDetailsVC") as! ItemDetailsVC
//            self.navigationController?.pushViewController(vc, animated: true)
//        }

    }
    
}
