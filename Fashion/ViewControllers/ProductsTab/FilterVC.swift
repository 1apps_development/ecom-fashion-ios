//
//  FilterVC.swift
//  Fashion
//
//  Created by DREAMWORLD on 28/03/22.
//

import UIKit

class FilterVC: UIViewController {
    @IBOutlet weak var tagListView: TagListView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tagListView.delegate = self
        tagListView.addTags(["Accessories", "Glasses", "Accessories", "Accessories", "Glasses", "Accessories", "Glasses","Glasses"])
        tagListView.alignment = .left
        tagListView.textFont = UIFont(name: "Outfit Medium", size: 14)!
        tagListView.borderWidths = 1
        tagListView.layer.borderWidth = 0.0
//        tagListView.tagViewHeight = 50.0
//        tagListView.shadowRadius = 4
       
    }
    @IBAction func btnTap_back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
}
extension FilterVC : TagListViewDelegate
{
    // MARK: TagListViewDelegate
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag pressed: \(title), \(sender)")
        tagView.isSelected = !tagView.isSelected
    }

    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag Remove pressed: \(title), \(sender)")
        sender.removeTagView(tagView)
    }
}

