//
//  SettingsVC.swift
//  Fashion
//
//  Created by DREAMWORLD on 25/03/22.
//

import UIKit

class SettingsCell : UITableViewCell
{
    
    @IBOutlet weak var lbl_subtitle: UILabel!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var img_images: UIImageView!
}
class SettingsVC: UIViewController {

    @IBOutlet weak var Tableview_SettingsList: UITableView!
    @IBOutlet weak var Height_Tableview: NSLayoutConstraint!
    
    var TitleArray = ["Edit your account information","Subscribe / unsubscribe newsletter","Your Reward Points","Recurring payments","Change your password","View your return requests","Modify your address book entries","Downloads","View your order history","Your Transactions","Loyality Program","My Returns"]
    
    var SubtitleArray = ["edit your account","subscribe for newsletter","Count Reward Point","See your Payment","Change Your Passowrd","See your Return","Edit your address","Download Your Theme","See your order history","See your Transaction","Get cash by following people","My return orders"]
    
    var ImagesArray = ["ic_user","ic_Subscribe","ic_Reward","ic_Recurring","ic_Changepwd","ic_returnrequests","ic_address","ic_Downloads","ic_orderhistory","ic_Transaction","ic_Loyality","ic_returnorder"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.Tableview_SettingsList.dataSource = self
        self.Tableview_SettingsList.delegate = self
        self.Tableview_SettingsList.reloadData()
        self.Height_Tableview.constant = CGFloat(TitleArray.count * 75)
    }
    

}
//MARK: Tableview Methods
extension SettingsVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TitleArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.Tableview_SettingsList.dequeueReusableCell(withIdentifier: "SettingsCell") as! SettingsCell
        cell.lbl_title.text = self.TitleArray[indexPath.row]
        cell.lbl_subtitle.text = self.SubtitleArray[indexPath.row]
        cell.img_images.image = UIImage(named: self.ImagesArray[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0
        {
            let vc = MainstoryBoard.instantiateViewController(withIdentifier: "PersonalDetailsVC") as! PersonalDetailsVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.row == 1
        {
            
        }
        else if indexPath.row == 3
        {
            
        }
        else if indexPath.row == 4
        {
            let vc = MainstoryBoard.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.row == 5
        {
            
        }
        else if indexPath.row == 6
        {
            let vc = MainstoryBoard.instantiateViewController(withIdentifier: "AddaddressVC") as! AddaddressVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.row == 7
        {
            
        }
        else if indexPath.row == 8
        {
            let vc = MainstoryBoard.instantiateViewController(withIdentifier: "OrderHistoryVC") as! OrderHistoryVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.row == 9
        {
           
        }
        else if indexPath.row == 10
        {
            
            let vc = MainstoryBoard.instantiateViewController(withIdentifier: "LoyalityprogramVC") as! LoyalityprogramVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.row == 11
        {
            let vc = MainstoryBoard.instantiateViewController(withIdentifier: "MyReturnsVC") as! MyReturnsVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
    }
    
}
