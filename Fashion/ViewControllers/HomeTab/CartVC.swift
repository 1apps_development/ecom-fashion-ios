//
//  CartVC.swift
//  Fashion
//
//  Created by DREAMWORLD on 24/03/22.
//

import UIKit

class CartListCell : UITableViewCell
{
    @IBOutlet weak var btn_pluse: UIButton!
    @IBOutlet weak var btn_Minus: UIButton!
    @IBOutlet weak var lbl_qty: UILabel!
}
class CartVC: UIViewController {

    @IBOutlet weak var Height_Tableview: NSLayoutConstraint!
    @IBOutlet weak var Tableview_CartList: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.Tableview_CartList.delegate = self
        self.Tableview_CartList.dataSource = self
        self.Tableview_CartList.reloadData()
        self.Height_Tableview.constant = 5 * 95
        
    }
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func btnTap_CheckOut(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BillingDetailsVC") as! BillingDetailsVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
//MARK: Tableview Methods
extension CartVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.Tableview_CartList.dequeueReusableCell(withIdentifier: "CartListCell") as! CartListCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    
    func tableView(_ tableView: UITableView,trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        
        let deleteAction = UIContextualAction(
            style: .normal,
            title:  nil,
            handler: { (_, _, success: (Bool) -> Void) in
                success(true)
                print("Your action in here")
            }
        )
       
        
        deleteAction.image = UISwipeActionsConfiguration.makeTitledImage(
            image: UIImage(named: "ic_trash"),
            title: "Delete")
        deleteAction.backgroundColor = UIColor.init(named: "App_Color")
        
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
}
extension UISwipeActionsConfiguration {
    
    public static func makeTitledImage(
        image: UIImage?,
        title: String,
        textColor: UIColor = UIColor.init(named: "App_bg_Color")!,
        font: UIFont = UIFont.init(name: "Outfit Medium", size: 12)!,
        size: CGSize = .init(width: 50, height: 50)
    ) -> UIImage? {
        
        /// Create attributed string attachment with image
        let attachment = NSTextAttachment()
        attachment.image = image
        let imageString = NSAttributedString(attachment: attachment)
        
        /// Create attributed string with title
        let text = NSAttributedString(
            string: "\n\(title)",
            attributes: [
                .foregroundColor: textColor,
                .font: font
            ]
        )
        
        /// Merge two attributed strings
        let mergedText = NSMutableAttributedString()
        mergedText.append(imageString)
        mergedText.append(text)
        
        /// Create label and append that merged attributed string
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        label.textAlignment = .center
        label.numberOfLines = 2
        label.attributedText = mergedText
        
        /// Create image from that label
        let renderer = UIGraphicsImageRenderer(bounds: label.bounds)
        let image = renderer.image { rendererContext in
            label.layer.render(in: rendererContext.cgContext)
        }
        
        /// Convert it to UIImage and return
        if let cgImage = image.cgImage {
            return UIImage(cgImage: cgImage, scale: UIScreen.main.scale, orientation: .up)
        }
        
        return nil
    }
}
