//
//  ItemDetailsVC.swift
//  Fashion
//
//  Created by DREAMWORLD on 24/03/22.
//

import UIKit

class SizeCell : UICollectionViewCell
{
    @IBOutlet weak var cell_view: UIView!
    
}
class RattingsListCell : UICollectionViewCell
{
    @IBOutlet weak var cell_view: UIView!
    
}
class ItemDetailsVC: UIViewController {

    @IBOutlet weak var Collectionview_sizeList: UICollectionView!
    @IBOutlet weak var Collectionview_RattingsList: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.Collectionview_sizeList.delegate = self
        self.Collectionview_sizeList.dataSource = self
        self.Collectionview_sizeList.reloadData()
        
        self.Collectionview_RattingsList.delegate = self
        self.Collectionview_RattingsList.dataSource = self
        self.Collectionview_RattingsList.reloadData()
        
    }
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnTap_Addtocart(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(identifier: "CartVC") as! CartVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
// MARK:- CollectionView Deleget methods
extension ItemDetailsVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.Collectionview_sizeList
        {
            return 4
        }
        else if collectionView == self.Collectionview_RattingsList
        {
            return 40
        }
        else
        {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.Collectionview_sizeList
        {
            let cell = self.Collectionview_sizeList.dequeueReusableCell(withReuseIdentifier: "SizeCell", for: indexPath) as! SizeCell
            cornerRadius(viewName: cell.cell_view, radius: cell.cell_view.frame.height / 2)
           
            return cell
        }
        else if collectionView == self.Collectionview_RattingsList
        {
            let cell = self.Collectionview_RattingsList.dequeueReusableCell(withReuseIdentifier: "RattingsListCell", for: indexPath) as! RattingsListCell
           
            return cell
        }
        else
        {
            return UICollectionViewCell()
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == self.Collectionview_sizeList
        {
            return CGSize(width: 40, height: 40)
        }
        else if collectionView == self.Collectionview_RattingsList
        {
            return CGSize(width: collectionView.bounds.size.width - 10, height: 260)
        }
        else
        {
            return CGSize.zero
        }
    }
    
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//
//        if collectionView == self.CollectionView_CategoriesList
//        {
//            let data = self.Categories_Array[indexPath.row]
//            let vc = self.storyboard?.instantiateViewController(identifier: "CategorieWiseVC") as! CategorieWiseVC
//            vc.category_id = data["id"].stringValue
//            vc.category_name = data["name"].stringValue
//            self.navigationController?.pushViewController(vc, animated: true)
//        }
//        else if collectionView == self.CollectionView_FirstBannerList
//        {
//            let data = self.TopBanner_Array[indexPath.row]
//            let vc = self.storyboard?.instantiateViewController(identifier: "RestaurantDetailsVC") as! RestaurantDetailsVC
//            vc.restaurant_id = data["restaurant_id"].stringValue
//            self.navigationController?.pushViewController(vc, animated: true)
//        }
//        else if collectionView == self.CollectionView_BannerList
//        {
//            let data = self.CenterBanner_Array[indexPath.row]
//            let vc = self.storyboard?.instantiateViewController(identifier: "RestaurantDetailsVC") as! RestaurantDetailsVC
//            vc.restaurant_id = data["restaurant_id"].stringValue
//            self.navigationController?.pushViewController(vc, animated: true)
//        }
//        else if collectionView == self.CollectionView_RestaurantList
//        {
//            let data = self.NearByRestaurants_Array[indexPath.row]
//            let vc = self.storyboard?.instantiateViewController(identifier: "RestaurantDetailsVC") as! RestaurantDetailsVC
//            vc.restaurant_id = data["id"].stringValue
//            self.navigationController?.pushViewController(vc, animated: true)
//        }
//        else if collectionView == self.CollectionView_RecommendedList
//        {
//            let data = self.RecomandedRestaurants_Array[indexPath.row]
//            let vc = self.storyboard?.instantiateViewController(identifier: "RestaurantDetailsVC") as! RestaurantDetailsVC
//            vc.restaurant_id = data["id"].stringValue
//            self.navigationController?.pushViewController(vc, animated: true)
//        }
//    }
    
}
