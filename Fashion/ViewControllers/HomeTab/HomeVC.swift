//
//  HomeVC.swift
//  Fashion
//
//  Created by DREAMWORLD on 23/03/22.
//

import UIKit

class featuredListCell : UICollectionViewCell
{
    @IBOutlet weak var cell_view: UIView!
    @IBOutlet weak var lbl_title: UILabel!
}
class featuredItemListCell : UICollectionViewCell
{
    @IBOutlet weak var cell_view: UIView!
}
class CategoriesCell : UICollectionViewCell
{
    @IBOutlet weak var cell_view: UIView!
}

class HomeVC: UIViewController {

    
    @IBOutlet weak var Collectionview_FeaturedList: UICollectionView!
    @IBOutlet weak var Collectionview_FeaturedItemList: UICollectionView!
    @IBOutlet weak var Collectionview_CategoriesList: UICollectionView!
    @IBOutlet weak var Collectionview_BestsellersList: UICollectionView!
    
    @IBOutlet weak var Height_FeaturedItemCollectionview: NSLayoutConstraint!
    @IBOutlet weak var Height_CategoriesCollectionview: NSLayoutConstraint!
    @IBOutlet weak var Height_BestsellersCollectionview: NSLayoutConstraint!
    
    var ProductArray = ["All Products","Glasses","Clothes","Hats"]
    override func viewDidLoad() {
        super.viewDidLoad()

        self.Collectionview_FeaturedList.delegate = self
        self.Collectionview_FeaturedList.dataSource = self
        self.Collectionview_FeaturedList.reloadData()
        
        self.Collectionview_FeaturedItemList.delegate = self
        self.Collectionview_FeaturedItemList.dataSource = self
        self.Collectionview_FeaturedItemList.reloadData()
        self.Height_FeaturedItemCollectionview.constant = ((UIScreen.main.bounds.width - 54) / 2) + 60
        
        self.Collectionview_CategoriesList.delegate = self
        self.Collectionview_CategoriesList.dataSource = self
        self.Collectionview_CategoriesList.reloadData()
        self.Height_CategoriesCollectionview.constant = 220
        
        self.Collectionview_BestsellersList.delegate = self
        self.Collectionview_BestsellersList.dataSource = self
        self.Collectionview_BestsellersList.reloadData()
        self.Height_BestsellersCollectionview.constant = ((UIScreen.main.bounds.width - 54) / 2) + 60
        
    }
    @IBAction func btnTap_Search(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchVC") as! SearchVC
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    @IBAction func btnTap_ShowCategories(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AllCategoriesVC") as! AllCategoriesVC
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    @IBAction func btnTap_Showbestsellers(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BestsellersVC") as! BestsellersVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
}
// MARK:- CollectionView Deleget methods
extension HomeVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.Collectionview_FeaturedList
        {
            return ProductArray.count
        }
        else if collectionView == self.Collectionview_FeaturedItemList
        {
            return 4
        }
        else if collectionView == self.Collectionview_CategoriesList
        {
            return 4
        }
        else if collectionView == self.Collectionview_BestsellersList
        {
            return 4
        }
        else
        {
            
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.Collectionview_FeaturedList
        {
            let cell = self.Collectionview_FeaturedList.dequeueReusableCell(withReuseIdentifier: "featuredListCell", for: indexPath) as! featuredListCell
            cell.lbl_title.text = self.ProductArray[indexPath.row]
            if indexPath.row == 0
            {
                cell.cell_view.backgroundColor = UIColor.init(named: "App_Color")
                cell.lbl_title.textColor = UIColor.init(named: "App_bg_Color")
                cornerRadius(viewName: cell.cell_view, radius: 10)
            }
            else{
                cell.cell_view.backgroundColor = UIColor.init(named: "App_bg_Color")
                cell.lbl_title.textColor = UIColor.white
                setBorder(viewName: cell.cell_view, borderwidth: 1, borderColor: UIColor.white.cgColor, cornerRadius: 10)
            }
            return cell
        }
        else if collectionView == self.Collectionview_FeaturedItemList
        {
            let cell = self.Collectionview_FeaturedItemList.dequeueReusableCell(withReuseIdentifier: "featuredItemListCell", for: indexPath) as! featuredItemListCell
            return cell
        }
        else if collectionView == self.Collectionview_CategoriesList
        {
            let cell = self.Collectionview_CategoriesList.dequeueReusableCell(withReuseIdentifier: "CategoriesCell", for: indexPath) as! CategoriesCell
            return cell
        }
        else if collectionView == self.Collectionview_BestsellersList
        {
            let cell = self.Collectionview_BestsellersList.dequeueReusableCell(withReuseIdentifier: "featuredItemListCell", for: indexPath) as! featuredItemListCell
            return cell
        }
        else
        {
            return UICollectionViewCell()
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == self.Collectionview_FeaturedList
        {
            return CGSize(width: (UIScreen.main.bounds.width - 54) / 2, height: 40)
        }
        else if collectionView == self.Collectionview_FeaturedItemList
        {
            return CGSize(width: (UIScreen.main.bounds.width - 54) / 2, height: ((UIScreen.main.bounds.width - 54) / 2) + 60)
        }
        else if collectionView == self.Collectionview_CategoriesList
        {
            return CGSize(width: (UIScreen.main.bounds.width - 54) / 2, height: 210)
        }
        else if collectionView == self.Collectionview_BestsellersList
        {
            return CGSize(width: (UIScreen.main.bounds.width - 54) / 2, height: ((UIScreen.main.bounds.width - 54) / 2) + 60)
        }
        else
        {
            return CGSize.zero
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        if collectionView == self.Collectionview_FeaturedItemList
        {
            let vc = self.storyboard?.instantiateViewController(identifier: "ItemDetailsVC") as! ItemDetailsVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if collectionView == self.Collectionview_BestsellersList
        {
            let vc = self.storyboard?.instantiateViewController(identifier: "ItemDetailsVC") as! ItemDetailsVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
}
