//
//  SearchVC.swift
//  Fashion
//
//  Created by DREAMWORLD on 29/03/22.
//

import UIKit

class SearchVC: UIViewController {
    @IBOutlet weak var Collectionview_SearchList: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.Collectionview_SearchList.dataSource = self
        self.Collectionview_SearchList.delegate = self
        self.Collectionview_SearchList.reloadData()
    }
    
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    

}
// MARK:- CollectionView Deleget methods
extension SearchVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         if collectionView == self.Collectionview_SearchList
        {
            return 10
        }
        else
        {
            
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         if collectionView == self.Collectionview_SearchList
        {
            let cell = self.Collectionview_SearchList.dequeueReusableCell(withReuseIdentifier: "featuredItemListCell", for: indexPath) as! featuredItemListCell
            return cell
        }
        else
        {
            return UICollectionViewCell()
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         if collectionView == self.Collectionview_SearchList
        {
            return CGSize(width: (UIScreen.main.bounds.width - 54) / 2, height: ((UIScreen.main.bounds.width - 54) / 2) + 60)
        }
        else
        {
            return CGSize.zero
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        if collectionView == self.Collectionview_SearchList
        {
            let vc = self.storyboard?.instantiateViewController(identifier: "ItemDetailsVC") as! ItemDetailsVC
            self.navigationController?.pushViewController(vc, animated: true)
        }

    }
    
}
