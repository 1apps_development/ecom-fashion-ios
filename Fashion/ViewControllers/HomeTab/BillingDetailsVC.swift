//
//  BillingDetailsVC.swift
//  Fashion
//
//  Created by DREAMWORLD on 25/03/22.
//

import UIKit

class BillingDetailsVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnTap_continue(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectPaymentVC") as! SelectPaymentVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
