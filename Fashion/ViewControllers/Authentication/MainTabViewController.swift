//
//  MainTabViewController.swift
//  StoreGo
//
//  Created by Gravity on 21/03/22.
//

import UIKit
import SOTabBar

//172C4E

class MainTabViewController: SOTabBarController {
    
    override func loadView() {
        super.loadView()
        SOTabBarSetting.tabBarHeight = 65
        SOTabBarSetting.tabBarBackground = UIColor.white
        SOTabBarSetting.tabBarTintColor = UIColor.init(named: "App_Color")!
//        SOTabBarSetting.tabBarTintColor = #colorLiteral(red: 0.4352941176, green: 0.8509803922, blue: 0.2
        SOTabBarSetting.tabBarCircleSize = CGSize(width: 50, height: 50)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        let tasksVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeVC")
        let invoicesVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AllProductsVC")
        let dashbordvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AllCategoriesVC")
        let usersvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WishlistVC")
        let settingsvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SettingsVC")
       
        let image = UIImage.init(named: "01")?.imageWithColor(color1: hexStringToUIColor(hex: "717887"))
        
        tasksVC.tabBarItem = UITabBarItem(title: "Home", image: image, selectedImage: UIImage(named: "01"))
        invoicesVC.tabBarItem = UITabBarItem(title: "Products", image: UIImage(named: "02"), selectedImage: UIImage(named: "02"))
        dashbordvc.tabBarItem = UITabBarItem(title: "Categories", image: UIImage(named: "05"), selectedImage: UIImage(named: "05"))
        usersvc.tabBarItem = UITabBarItem(title: "Wishlist", image: UIImage(named: "03"), selectedImage: UIImage(named: "03"))
        settingsvc.tabBarItem = UITabBarItem(title: "Settings", image: UIImage(named: "04"), selectedImage: UIImage(named: "04"))
           
        viewControllers = [tasksVC, invoicesVC,dashbordvc,usersvc,settingsvc]
        
//        NotificationCenter.default.addObserver(self, selector: #selector(self.showSpinningWheel(_:)), name: NSNotification.Name(rawValue: "tab_notification"), object: nil)

    }
//    @objc func showSpinningWheel(_ notification: NSNotification) {
//
//        if let indx = notification.userInfo?["tab"] as? Int {
//            self.tabBar(SOTabBar(), didSelectTabAt: indx)
//        }
//    }

    
}

extension MainTabViewController: SOTabBarControllerDelegate {
    func tabBarController(_ tabBarController: SOTabBarController, didSelect viewController: UIViewController) {
//        print(viewController.tabBarItem.title ?? "")
    }
}
extension UIImage {
    func imageWithColor(color1: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        color1.setFill()

        let context = UIGraphicsGetCurrentContext()
        context?.translateBy(x: 0, y: self.size.height)
        context?.scaleBy(x: 1.0, y: -1.0)
        context?.setBlendMode(CGBlendMode.normal)

        let rect = CGRect(origin: .zero, size: CGSize(width: self.size.width, height: self.size.height))
        context?.clip(to: rect, mask: self.cgImage!)
        context?.fill(rect)

        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage!
    }
}
