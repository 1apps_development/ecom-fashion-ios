//
//  ForgotpasswordVC.swift
//  Fashion
//
//  Created by DREAMWORLD on 29/03/22.
//

import UIKit

class ForgotpasswordVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func btnTap_SendCode(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OTPVC") as! OTPVC
        self.navigationController?.pushViewController(vc, animated: true)
    }

}
