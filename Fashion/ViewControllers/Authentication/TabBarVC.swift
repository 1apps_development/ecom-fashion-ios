//
//  TabBarVC.swift
//  Fashion
//
//  Created by DREAMWORLD on 25/03/22.
//

import UIKit

class TabBarVC: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        UITabBarItem.appearance()
            .setTitleTextAttributes(
                [NSAttributedString.Key.font: UIFont(name: "Outfit Medium", size: 14)!],
            for: .normal)
        
    }
    

}
