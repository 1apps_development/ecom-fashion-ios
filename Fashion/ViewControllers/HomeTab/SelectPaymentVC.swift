//
//  SelectPaymentVC.swift
//  Fashion
//
//  Created by DREAMWORLD on 25/03/22.
//

import UIKit
class PaymentCell : UITableViewCell
{
    
}
class SelectPaymentVC: UIViewController {

    @IBOutlet weak var Height_Tableview: NSLayoutConstraint!
    @IBOutlet weak var Tableview_PaymentList: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.Tableview_PaymentList.delegate = self
        self.Tableview_PaymentList.dataSource = self
        self.Tableview_PaymentList.reloadData()
        self.Height_Tableview.constant = 2 * 95
       
    }
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnTap_Continue(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectDeliveryVC") as! SelectDeliveryVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
//MARK: Tableview Methods
extension SelectPaymentVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.Tableview_PaymentList.dequeueReusableCell(withIdentifier: "PaymentCell") as! PaymentCell
        return cell
    }
    
}
