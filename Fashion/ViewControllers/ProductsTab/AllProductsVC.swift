//
//  AllProductsVC.swift
//  Fashion
//
//  Created by DREAMWORLD on 25/03/22.
//

import UIKit

class AllProductsVC: UIViewController {

    @IBOutlet weak var Collectionview_ProductsList: UICollectionView!
    @IBOutlet weak var Collectionview_ProductsListing: UICollectionView!
    @IBOutlet weak var Height_ProductsListingCollectionview: NSLayoutConstraint!
    @IBOutlet weak var Height_ProductsListCollectionview: NSLayoutConstraint!
    var ProductArray = ["All Products","Glasses","Clothes","Hats"]
    override func viewDidLoad() {
        super.viewDidLoad()

        self.Collectionview_ProductsListing.delegate = self
        self.Collectionview_ProductsListing.dataSource = self
        self.Collectionview_ProductsListing.reloadData()
        self.Height_ProductsListingCollectionview.constant = 120
        
        self.Collectionview_ProductsList.delegate = self
        self.Collectionview_ProductsList.dataSource = self
        self.Collectionview_ProductsList.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.Height_ProductsListCollectionview.constant = 10 * self.Collectionview_ProductsListing.contentSize.height + 40
        }
        
    }
    
    
    @IBAction func btnTap_filter(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
}
// MARK:- CollectionView Deleget methods
extension AllProductsVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.Collectionview_ProductsListing
        {
            return ProductArray.count
        }
        else if collectionView == self.Collectionview_ProductsList
        {
            return 10
        }
        else
        {
            
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.Collectionview_ProductsListing
        {
            let cell = self.Collectionview_ProductsListing.dequeueReusableCell(withReuseIdentifier: "featuredListCell", for: indexPath) as! featuredListCell
            cell.lbl_title.text = self.ProductArray[indexPath.row]
            if indexPath.row == 0
            {
                cell.cell_view.backgroundColor = UIColor.init(named: "App_Color")
                cell.lbl_title.textColor = UIColor.init(named: "App_bg_Color")
                cornerRadius(viewName: cell.cell_view, radius: 10)
            }
            else{
                cell.cell_view.backgroundColor = UIColor.init(named: "App_bg_Color")
                cell.lbl_title.textColor = UIColor.white
                setBorder(viewName: cell.cell_view, borderwidth: 1, borderColor: UIColor.white.cgColor, cornerRadius: 10)
            }
            return cell
        }
        else if collectionView == self.Collectionview_ProductsList
        {
            let cell = self.Collectionview_ProductsList.dequeueReusableCell(withReuseIdentifier: "featuredItemListCell", for: indexPath) as! featuredItemListCell
            return cell
        }
        else
        {
            return UICollectionViewCell()
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == self.Collectionview_ProductsListing
        {
            return CGSize(width: (UIScreen.main.bounds.width - 72) / 4, height: 100)
        }
        else if collectionView == self.Collectionview_ProductsList
        {
            return CGSize(width: (UIScreen.main.bounds.width - 54) / 2, height: ((UIScreen.main.bounds.width - 54) / 2) + 60)
        }
        else
        {
            return CGSize.zero
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        if collectionView == self.Collectionview_ProductsList
        {
            let vc = self.storyboard?.instantiateViewController(identifier: "ItemDetailsVC") as! ItemDetailsVC
            self.navigationController?.pushViewController(vc, animated: true)
        }

    }
    
}
