//
//  WishlistVC.swift
//  Fashion
//
//  Created by DREAMWORLD on 29/03/22.
//

import UIKit

class WishlistVC: UIViewController {
    @IBOutlet weak var Collectionview_ProductsList: UICollectionView!
    @IBOutlet weak var Height_ProductsListCollectionview: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.Collectionview_ProductsList.delegate = self
        self.Collectionview_ProductsList.dataSource = self
        self.Collectionview_ProductsList.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.Height_ProductsListCollectionview.constant = self.Collectionview_ProductsList.contentSize.height
        }
    }
   
    
    @IBAction func btnTap_filter(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
        self.navigationController?.pushViewController(vc, animated: false)
    }

}
// MARK:- CollectionView Deleget methods
extension WishlistVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         if collectionView == self.Collectionview_ProductsList
        {
            return 10
        }
        else
        {
            
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         if collectionView == self.Collectionview_ProductsList
        {
            let cell = self.Collectionview_ProductsList.dequeueReusableCell(withReuseIdentifier: "featuredItemListCell", for: indexPath) as! featuredItemListCell
            return cell
        }
        else
        {
            return UICollectionViewCell()
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         if collectionView == self.Collectionview_ProductsList
        {
            return CGSize(width: (UIScreen.main.bounds.width - 54) / 2, height: ((UIScreen.main.bounds.width - 54) / 2) + 60)
        }
        else
        {
            return CGSize.zero
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        if collectionView == self.Collectionview_ProductsList
        {
            let vc = self.storyboard?.instantiateViewController(identifier: "ItemDetailsVC") as! ItemDetailsVC
            self.navigationController?.pushViewController(vc, animated: true)
        }

    }
    
}
