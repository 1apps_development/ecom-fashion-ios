//
//  OrderhistoryDetailsVC.swift
//  Fashion
//
//  Created by DREAMWORLD on 28/03/22.
//

import UIKit

class OrderhistoryDetailsVC: UIViewController {

    @IBOutlet weak var Height_Tableview: NSLayoutConstraint!
    @IBOutlet weak var Tableview_OrderhistoryDetailsOrderList: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.Tableview_OrderhistoryDetailsOrderList.delegate = self
        self.Tableview_OrderhistoryDetailsOrderList.dataSource = self
        self.Tableview_OrderhistoryDetailsOrderList.reloadData()
        self.Height_Tableview.constant = 5 * 95
    }
    @IBAction func btnTap_Back(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }


}
//MARK: Tableview Methods
extension OrderhistoryDetailsVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.Tableview_OrderhistoryDetailsOrderList.dequeueReusableCell(withIdentifier: "CartListCell") as! CartListCell
        return cell
    }
    
}
