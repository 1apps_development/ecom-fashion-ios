//
//  ConfirmOrderVC.swift
//  Fashion
//
//  Created by DREAMWORLD on 25/03/22.
//

import UIKit

class ConfirmOrderVC: UIViewController {

    @IBOutlet weak var Height_Tableview: NSLayoutConstraint!
    @IBOutlet weak var Tableview_ConfirmproductsList: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.Tableview_ConfirmproductsList.delegate = self
        self.Tableview_ConfirmproductsList.dataSource = self
        self.Tableview_ConfirmproductsList.reloadData()
        self.Height_Tableview.constant = 5 * 95
        
    }
    @IBAction func btnTap_Back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnTap_Confirm(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OrderPlaceSucessVC") as! OrderPlaceSucessVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

}
//MARK: Tableview Methods
extension ConfirmOrderVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.Tableview_ConfirmproductsList.dequeueReusableCell(withIdentifier: "CartListCell") as! CartListCell
        return cell
    }
    
}
